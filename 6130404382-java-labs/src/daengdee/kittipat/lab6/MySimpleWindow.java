package daengdee.kittipat.lab6;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: March 2, 2019
 */
import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MySimpleWindow extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JButton okButton;
	protected JButton cancelButton;
	protected JPanel okCancelPanel;
	protected JPanel windowPanel;

	public MySimpleWindow(String title) {
		super(title);
	}

	protected void setFrameFeatures() {
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setVisible(true);

	}

	protected void addComponents() {
		okButton = new JButton("OK");
		cancelButton = new JButton("Cancel");
		okCancelPanel = new JPanel();
		okCancelPanel.add(cancelButton);
		okCancelPanel.add(okButton);

		windowPanel = (JPanel) this.getContentPane();
		windowPanel.setLayout(new BorderLayout());
		windowPanel.add(okCancelPanel, BorderLayout.CENTER);

	}

	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
