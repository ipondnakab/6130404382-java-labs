package daengdee.kittipat.lab6;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: March 2, 2019
 */
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV1 extends MySimpleWindow {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JLabel brandNameLabel;
	protected JLabel modelNameLabel;
	protected JLabel weightLabel;
	protected JLabel priceLabel;
	protected JLabel OSLabel;
	protected JTextField brandNameTextField;
	protected JTextField modelNameTextField;
	protected JTextField weightTextField;
	protected JTextField priceTextField;
	protected JRadioButton androidRB;
	protected JRadioButton iOSRB;
	protected JPanel topPanel;
	protected JPanel buttomContains;
	protected ButtonGroup group;

	public MobileDeviceFormV1(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();
		brandNameLabel = new JLabel(" Brand name:");
		modelNameLabel = new JLabel(" Model name:");
		weightLabel = new JLabel(" Weight (kg.):");
		priceLabel = new JLabel(" Price (Baht):");
		OSLabel = new JLabel(" Mobile OS:");
		brandNameTextField = new JTextField(15);
		modelNameTextField = new JTextField(15);
		weightTextField = new JTextField(15);
		priceTextField = new JTextField(15);

		androidRB = new JRadioButton("Android");
		iOSRB = new JRadioButton("iOS");
		topPanel = new JPanel();
		topPanel.setLayout(new GridLayout(0, 2));
		group = new ButtonGroup();
		group.add(iOSRB);
		group.add(androidRB);

		JPanel OSGroup = new JPanel();
		OSGroup.setLayout(new FlowLayout());
		OSGroup.add(androidRB);
		OSGroup.add(iOSRB);

		topPanel.add(brandNameLabel);
		topPanel.add(brandNameTextField);
		topPanel.add(modelNameLabel);
		topPanel.add(modelNameTextField);
		topPanel.add(weightLabel);
		topPanel.add(weightTextField);
		topPanel.add(priceLabel);
		topPanel.add(priceTextField);
		topPanel.add(OSLabel);
		topPanel.add(OSGroup);

		windowPanel = (JPanel) this.getContentPane();
		windowPanel.setLayout(new BorderLayout());
		windowPanel.add(topPanel, BorderLayout.NORTH);
		windowPanel.add(okCancelPanel, BorderLayout.SOUTH);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV1 MobileDeviceFormV1 = new MobileDeviceFormV1("Mobile Device Form V1");
		MobileDeviceFormV1.addComponents();
		MobileDeviceFormV1.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
