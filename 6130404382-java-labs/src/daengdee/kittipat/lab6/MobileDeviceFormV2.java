package daengdee.kittipat.lab6;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: March 2, 2019
 */
 
import java.awt.BorderLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV2 extends MobileDeviceFormV1 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JLabel typeLabel;
	protected JLabel reviewLabel;
	protected JTextArea reviewTextArea;
	protected JComboBox<?> typeCombobox;
	protected JPanel bottomPanel, reviewPanel;
	protected JScrollPane scroll;
	protected String[] types = { "Phone", "Tablet", "Smart TV" };

	public MobileDeviceFormV2(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	protected void addComponents() {
		super.addComponents();
		typeLabel = new JLabel(" Type:");
		reviewLabel = new JLabel(" Review:");
		reviewTextArea = new JTextArea();

		typeCombobox = new JComboBox<Object>(types);
		topPanel.add(typeLabel);
		topPanel.add(typeCombobox);

		reviewPanel = new JPanel(new BorderLayout());
		reviewTextArea = new JTextArea(2, 35);
		reviewTextArea.setEditable(true); // set textArea non-editable
		scroll = new JScrollPane(reviewTextArea);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		reviewTextArea.setText("Bigger than previous Note phones in every way, "
				+ "the Samsung Galaxy Note 9 has a larger 6.4-inch screen, "
				+ "heftier 4,000mAh battery, and a massive 1TB of storage option.");
		reviewTextArea.setLineWrap(true);
		reviewTextArea.setWrapStyleWord(true);
		reviewTextArea.setAutoscrolls(true);
		reviewPanel.add(reviewLabel, BorderLayout.NORTH);
		reviewPanel.add(scroll, BorderLayout.CENTER);

		bottomPanel = new JPanel(new BorderLayout());
		bottomPanel.add(reviewPanel, BorderLayout.CENTER);

		windowPanel = (JPanel) this.getContentPane();
		windowPanel.setLayout(new BorderLayout());
		windowPanel.add(topPanel, BorderLayout.NORTH);
		windowPanel.add(bottomPanel, BorderLayout.CENTER);
		windowPanel.add(okCancelPanel, BorderLayout.SOUTH);

	}

	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV2 MobileDeviceFormV2 = new MobileDeviceFormV2("Mobile Device Form V2");
		MobileDeviceFormV2.addComponents();
		MobileDeviceFormV2.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
