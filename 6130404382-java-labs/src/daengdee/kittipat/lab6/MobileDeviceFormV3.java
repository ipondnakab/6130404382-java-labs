package daengdee.kittipat.lab6;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: March 2, 2019
 */
import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV3 extends MobileDeviceFormV2 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JMenuBar menuBar = new JMenuBar();
	protected JMenu menuFile = new JMenu("File");
	protected JMenu menuConfig = new JMenu("Config");
	protected JMenuItem menuFile_new, menuFile_Open, menuFile_Save, menuFile_Exit, menuConfig_Color, menuConfig_Size;
	protected JList<?> featuresList;
	protected String[] listFeatures = { "Design and build quality", "Great Camera", "Screen", "Battery Life" };
	protected JLabel featuresLable;
	protected JPanel featuresPanel;

	public MobileDeviceFormV3(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	protected void addComponents() {
		super.addComponents();
		featuresList = new JList(listFeatures);
		featuresLable = new JLabel("Features: ");
		featuresPanel = new JPanel(new GridLayout(0, 2));
		featuresPanel.add(featuresLable);
		featuresPanel.add(featuresList);
		bottomPanel.add(featuresPanel, BorderLayout.NORTH);
		windowPanel = (JPanel) this.getContentPane();
		windowPanel.setLayout(new BorderLayout());
		windowPanel.add(topPanel, BorderLayout.NORTH);
		windowPanel.add(bottomPanel, BorderLayout.CENTER);
		windowPanel.add(okCancelPanel, BorderLayout.SOUTH);
	}

	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	protected void addMenus() {
		setJMenuBar(menuBar);

		menuFile_new = new JMenuItem("New");
		menuFile_Open = new JMenuItem("Open");
		menuFile_Save = new JMenuItem("Save");
		menuFile_Exit = new JMenuItem("Exit");
		menuConfig_Color = new JMenuItem("Color");
		menuConfig_Size = new JMenuItem("Size");

		menuFile.add(menuFile_new);
		menuFile.add(menuFile_Open);
		menuFile.add(menuFile_Save);
		menuFile.add(menuFile_Exit);
		menuConfig.add(menuConfig_Color);
		menuConfig.add(menuConfig_Size);

		menuBar.add(menuFile);
		menuBar.add(menuConfig);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV3 mobileDeviceForm3 = new MobileDeviceFormV3("Mobile Device Form V3");
		mobileDeviceForm3.addMenus();
		mobileDeviceForm3.addComponents();
		mobileDeviceForm3.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
