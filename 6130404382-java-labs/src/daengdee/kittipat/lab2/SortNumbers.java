package daengdee.kittipat.lab2;
/* accept five decimals, read and store those numbers in an array, 
 * and then use Java subroutine to sort these numbers.
 * 
 * Author: Kittipat Daengdee
 * ID: 613040438-2
 * Sec: 1
 * Date: January 21, 2019
 */
import java.util.Arrays; 
public class SortNumbers {
	
	public static void main(String[] args) {
		if(args.length != 5) {
			System.err.println("Input error: enter 5 argument");
		}
		else {
			float first = Float.parseFloat(args[0]);
			float second = Float.parseFloat(args[1]);
			float third = Float.parseFloat(args[2]);
			float fourth = Float.parseFloat(args[3]);
			float fifth = Float.parseFloat(args[4]);
			Float[] listNumber = {first, second, third, fourth, fifth};
			Arrays.sort(listNumber);
			for (int i = 0; i < listNumber.length; i++) {
				   System.out.print(listNumber[i] + " ");
			}
		}
	}
}
