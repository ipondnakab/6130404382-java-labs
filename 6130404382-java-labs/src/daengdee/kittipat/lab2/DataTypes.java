package daengdee.kittipat.lab2;
/* Author: Kittipat Daengdee
 * ID: 613040438-2
 * Sec: 1
 * Date: January 21, 2019
 */
public class DataTypes {

	public static void main(String[] args) {
		
		String name = new String("kittipat Daengdee"); //name (firstname lastname)
		String ID = new String("6130404382"); // student ID
		char first = name.charAt(0);  // the first letter of your first name
		boolean booleanTrue = true; // boolean variable
		int twoLastOctal = 0122; // the last two digits of student ID number (declare a variable with an octal value)
		int twoLastHexadecimal = 0x52; // the last two digits of student ID number (declare a variable with a hexadecimal value)
		long twoLast = 82l;// the last two digits of student ID number
		float floatTwoLastPointTwoFirst = 82.61f; //the last two digits of your ID number and has the floating point of the first two digits of your ID number
		double doubleTwoLastPointTwoFirst = 82.61d; //the last two digits of your ID number and has the floating point of the first two digits of your ID number
		System.out.println("My name is" + name + "\n" + "My student is " + ID );
		System.out.println(first+ " " + booleanTrue + " " + twoLastOctal + " " + twoLastHexadecimal);
		System.out.println(twoLast + " " + floatTwoLastPointTwoFirst + " " + doubleTwoLastPointTwoFirst);
	}

}
