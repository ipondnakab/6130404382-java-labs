package daengdee.kittipat.lab2;
/* Compute how much money Wanlee has. 
 * you will input the number of notes of 1,000 Baht, 500 Baht, 100 Baht and 20 Baht, 
 * the program should output the total amount of money she has.
 * 
 * Author: Kittipat Daengdee
 * ID: 613040438-2
 * Sec: 1
 * Date: January 21, 2019
 */
public class ComputeMoney {

	public static void main(String[] args) {
		if (args.length != 4) {
			System.err.println("ComputeMoney <1,000 Bath> <5000 Bath> <100 Bath> <20 Bath>");
		}
		else {
			float thousand =  Float.parseFloat(args[0]);
			float fiveHundred = Float.parseFloat(args[1]);
			float hundred = Float.parseFloat(args[2]);
			float twenty = Float.parseFloat(args[3]);
			
			float total = (thousand*1000)+(fiveHundred*500)+(hundred*100)+(twenty*20);
			
			System.out.println("Total Money is "+ total);
		}
	}

}
