package daengdee.kittipat.lab7;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: March 3, 2019
 */
import java.awt.Color;
import java.awt.Font;

import javax.swing.SwingUtilities;

public class MobileDeviceFormV5 extends MobileDeviceFormV4 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MobileDeviceFormV5(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	protected void addComponents() {
		super.addComponents();
		brandNameLabel.setFont(new Font("Serif", Font.PLAIN, 14));
		modelNameLabel.setFont(new Font("Serif", Font.PLAIN, 14));
		weightLabel.setFont(new Font("Serif", Font.PLAIN, 14));
		priceLabel.setFont(new Font("Serif", Font.PLAIN, 14));
		OSLabel.setFont(new Font("Serif", Font.PLAIN, 14));
		featuresLable.setFont(new Font("Serif", Font.PLAIN, 14));
		reviewLabel.setFont(new Font("Serif", Font.PLAIN, 14));
		typeLabel.setFont(new Font("Serif", Font.PLAIN, 14));

		brandNameTextField.setFont(new Font("Serif", Font.BOLD, 14));
		modelNameTextField.setFont(new Font("Serif", Font.BOLD, 14));
		weightTextField.setFont(new Font("Serif", Font.BOLD, 14));
		priceTextField.setFont(new Font("Serif", Font.BOLD, 14));
		reviewTextArea.setFont(new Font("Serif", Font.BOLD, 14));

		okButton.setForeground(Color.BLUE);
		cancelButton.setForeground(Color.RED);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV5 mobileDeviceForm5 = new MobileDeviceFormV5("Mobile Device Form V5");
		mobileDeviceForm5.addMenus();
		mobileDeviceForm5.addComponents();
		mobileDeviceForm5.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
