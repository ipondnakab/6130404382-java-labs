package daengdee.kittipat.lab7;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: March 3, 2019
 */
import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

public class MobileDeviceV6 extends MobileDeviceFormV5 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JLabel image_1;

	public MobileDeviceV6(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	protected void addComponents() {
		super.addComponents();

		image_1 = new JLabel(new ImageIcon("images/galaxyNote9.jpg"));

		bottomPanel.add(image_1, BorderLayout.SOUTH);

	}

	public static void createAndShowGUI() {
		MobileDeviceV6 mobileDeviceForm6 = new MobileDeviceV6("Mobile Device Form V6");
		mobileDeviceForm6.addMenus();
		mobileDeviceForm6.addComponents();
		mobileDeviceForm6.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
