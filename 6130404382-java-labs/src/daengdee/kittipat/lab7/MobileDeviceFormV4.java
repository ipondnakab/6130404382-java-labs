package daengdee.kittipat.lab7;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: March 3, 2019
 */
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import daengdee.kittipat.lab6.MobileDeviceFormV3;

public class MobileDeviceFormV4 extends MobileDeviceFormV3 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JMenu subMenuConfig_Color, subMenuConfig_Size;
	protected JMenuItem menuItem_Size16, menuItem_Size20, menuItem_Size24, menuItem_ColorRed, menuItem_ColorGreen,
			menuItem_ColorBlue;

	public MobileDeviceFormV4(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV4 mobileDeviceForm4 = new MobileDeviceFormV4("Mobile Device Form V4");
		mobileDeviceForm4.addMenus();
		mobileDeviceForm4.addComponents();
		mobileDeviceForm4.setFrameFeatures();
	}

	protected void setFrameFeatures() {
		super.setFrameFeatures();

	}

	protected void addComponents() {
		super.addComponents();

	}

	protected void addMenus() {
		super.addMenus();
		updateMenuIcon();
		addSubMenus();

	}

	protected void addSubMenus() {
		menuConfig.remove(menuConfig_Color);
		menuConfig.remove(menuConfig_Size);

		subMenuConfig_Color = new JMenu("Color");
		subMenuConfig_Size = new JMenu("Size");
		menuConfig.add(subMenuConfig_Color);
		menuConfig.add(subMenuConfig_Size);

		menuItem_ColorRed = new JMenuItem("Red");
		menuItem_ColorGreen = new JMenuItem("Green");
		menuItem_ColorBlue = new JMenuItem("Blue");
		subMenuConfig_Color.add(menuItem_ColorRed);
		subMenuConfig_Color.add(menuItem_ColorGreen);
		subMenuConfig_Color.add(menuItem_ColorBlue);

		menuItem_Size16 = new JMenuItem("16");
		menuItem_Size20 = new JMenuItem("20");
		menuItem_Size24 = new JMenuItem("24");
		subMenuConfig_Size.add(menuItem_Size16);
		subMenuConfig_Size.add(menuItem_Size20);
		subMenuConfig_Size.add(menuItem_Size24);

	}

	protected void updateMenuIcon() {
		menuFile_new.setIcon(new ImageIcon("images/new.jpg"));

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
