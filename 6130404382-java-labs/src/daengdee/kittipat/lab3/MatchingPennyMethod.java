package daengdee.kittipat.lab3;

/* This Java program is head guess game
 * Author: Kittipat Daengdee 
 * ID: 613040438-2 
 * Sec: 1 
 * Date: January 31, 2019
 */
import java.util.Scanner;

public class MatchingPennyMethod {
	// This method accept input answer
	static String acceptInput() {
		String word;
		while (true) {
			System.out.print("Enter head or tail: ");
			Scanner sc = new Scanner(System.in);
			word = sc.nextLine();
			if (word.equalsIgnoreCase("head") || word.equalsIgnoreCase("tail")) {
				break;
			} else if (word.equalsIgnoreCase("exit")) {
				System.out.print("Good bye");
				sc.close();
				System.exit(0);
			} else {
				System.err.print("\nIncorrect input. head or tail only!!\n");
			}
		}
		return word;
	}

	// This method random computer play
	static int genComChoice() {
		int rand = (int) (Math.random() * 2);
		return rand;
	}

	// This method display Winer
	static void displayWiner(String word, int rand) {
		System.out.println("You play " + word);
		if (rand == 1) {
			System.out.println("Computer play head");
			if (word.equalsIgnoreCase("head")) {
				System.out.println("You win!! \n");
			} else {
				System.out.println("Computer wins!! \n");
			}

		} else if (rand == 0) {
			System.out.println("Computer play tail");
			if (word.equalsIgnoreCase("tail")) {

				System.out.println("You win!!\n");
			} else {
				System.out.println("Computer wins!! \n");
			}
		}
	}

	public static void main(String[] args) {
		while (true) {
			displayWiner(acceptInput(), genComChoice());

		}

	}

}
