package daengdee.kittipat.lab3;

import java.util.Scanner;

public class TypingTest {

	public static void main(String[] args) {
		String[] color = { "YELLOW", "RED", "BLUE", "INDIGO", "VIOLET", "GREEN", "ORANGE" };
		String colorAll = "";
		for (int i = 0; i < 8; i++) {
			int rand = (int) (Math.random() * 7);
			if (i == 7) {
				colorAll += color[rand];
			} 
			else {
				colorAll += color[rand] + " ";
			}
		}
		System.out.println(colorAll);
		double time = System.currentTimeMillis();
		while (true) {
			System.out.print("Typeyour answer: ");
			Scanner sc = new Scanner(System.in);
			String enter = sc.nextLine();
			if (enter.equalsIgnoreCase(colorAll)) {
				System.out.print("Your time is");
				double timeEnd = (System.currentTimeMillis() - time) / 1000;
				System.out.println(timeEnd);
				if (time < 12.0) {
					System.out.println("You type faster than average person");
					sc.close();
					System.exit(0);
				} 
				else {
					System.out.println("You type slower than average person");
					sc.close();
					System.exit(0);
				}
			}
		}

	}

}
