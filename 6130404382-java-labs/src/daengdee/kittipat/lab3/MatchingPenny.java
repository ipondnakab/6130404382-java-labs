package daengdee.kittipat.lab3;

import java.util.Scanner;

public class MatchingPenny {
	public static void main(String[] args) {
		String word;
		while (true) {
			System.out.print("\nEnter head or tail: ");
			Scanner sc = new Scanner(System.in);
			word = sc.nextLine();
			
			if (word.equalsIgnoreCase("head") || word.equalsIgnoreCase("tail")) {
				System.out.println("You play " + word);
				int rand = (int) (Math.random() * 2);
				if (rand == 1) {
					System.out.println("Computer play head");
					if (word.equalsIgnoreCase("head")) {
						System.out.println("You win!! \n");
					} else {
						System.out.println("Computer wins!! \n");
					}

				} else if (rand == 0) {
					System.out.println("Computer play tail");
					if (word.equalsIgnoreCase("tail")) {

						System.out.println("You win!!\n");
					} else {
						System.out.println("Computer wins!! \n");
					}
				}
			}

			else if (word.equalsIgnoreCase("exit")) {
				System.out.print("Good bye");
				sc.close();
				System.exit(0);
			} else {
				System.err.print("\nIncorrect input. head or tail only!!\n");
			}

		}

	}

}
