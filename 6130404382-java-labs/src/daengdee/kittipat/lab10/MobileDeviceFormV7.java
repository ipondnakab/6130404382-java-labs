package daengdee.kittipat.lab10;
/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 22, 2019
 */
 

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import daengdee.kittipat.lab7.MobileDeviceV6;

public class MobileDeviceFormV7 extends MobileDeviceV6  implements ActionListener, ListSelectionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String featuresSelec = "";
	
	protected String OSSelec;
	public MobileDeviceFormV7(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	protected void addComponents() {
		super.addComponents();
		brandNameTextField.setText("Samsung");
		modelNameTextField.setText("Galaxy Note 9");
		weightTextField.setText("201");
		priceTextField.setText("25500");
		reviewTextArea.setText("Note 9 has a larger 6.4-inch screen,  heftier 4,000mAh battery, and a\r\n" + 
				"massive 1TB of storage option");
		typeCombobox.setSelectedIndex(0);
		androidRB.setSelected(true);
		featuresList.setSelectedIndex(2);
		
		

	}

	protected void handleCancelButton() {
		brandNameTextField.setText("");
		modelNameTextField.setText("");
		weightTextField.setText("");
		priceTextField.setText("");
		reviewTextArea.setText("");
		group.clearSelection();
		featuresList.clearSelection();
	}
	protected void handleOKButton() {
		
		OSSelec = "";
		for(int i = 0; i < featuresList.getSelectedIndices().length; i++ ) {
			featuresSelec += (String)listFeatures[featuresList.getSelectedIndices()[i]] ;
			if(i < featuresList.getSelectedIndices().length-1) {
				featuresSelec += ", ";
			}
			
		}
		if (iOSRB.isSelected()){
			OSSelec = iOSRB.getText();
		}else if (androidRB.isSelected()) {
			OSSelec = androidRB.getText();
		}
		try {
		
		JOptionPane.showMessageDialog(null, brandNameLabel.getText()  + " " + brandNameTextField.getText() + modelNameLabel.getText() 
		+ " " + modelNameTextField.getText() + " " + weightLabel.getText() + " " + weightTextField.getText() + priceLabel.getText() + " " + priceTextField.getText()
		+ "\n" + OSLabel.getText() + " " + OSSelec + "\n" + typeLabel.getText() + " " + types[typeCombobox.getSelectedIndex()] 
		+ "\n" + " " + featuresLable.getText() + featuresSelec + "\n" + reviewLabel.getText() + " " + reviewTextArea.getText());
		}catch (Exception e) {
			System.out.println("Select" + OSLabel.getText());
		}
		
	}
	protected void addListeners() {
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		iOSRB.addActionListener(this);
		androidRB.addActionListener(this);
		featuresList.addListSelectionListener(this);
		typeCombobox.addActionListener(this);
		
		
		
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV7 mobileDeviceForm7 = new MobileDeviceFormV7("Mobile Device Form V7");
		mobileDeviceForm7.addMenus();
		mobileDeviceForm7.addComponents();
		mobileDeviceForm7.setFrameFeatures();
		mobileDeviceForm7.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	@Override
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		
		if (src == okButton) {
			handleOKButton();
		} else if (src == cancelButton) {
			handleCancelButton();
		} else if (src == iOSRB || src == androidRB) {
			if (iOSRB.isSelected()){
				JOptionPane.showMessageDialog(null,"Your os platform is now changed to IOS ");
			}else if (androidRB.isSelected()) {
				JOptionPane.showMessageDialog(null,"Your os platform is now changed to Android ");
			}
		
		}else if(src == typeCombobox) {
			JOptionPane.showMessageDialog(null,"Type is updated to " + types[typeCombobox.getSelectedIndex()]);
		}
		
	}
	@Override
	public void valueChanged(ListSelectionEvent e) {
		for(int i = 0; i < featuresList.getSelectedIndices().length; i++ ) {
			featuresSelec += (String)listFeatures[featuresList.getSelectedIndices()[i]] ;
			if(i < featuresList.getSelectedIndices().length-1) {
				featuresSelec += ", ";
			}
		
		}
		JOptionPane.showMessageDialog(null,featuresSelec);
		featuresSelec = "";
	}


}
