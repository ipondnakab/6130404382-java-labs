package daengdee.kittipat.lab10;

import java.awt.Color;
import java.awt.event.ActionEvent;
/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 22, 2019
 */
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV9 extends MobileDeviceFormV8 implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JFileChooser fileChooser;
	protected JColorChooser colorChooser;

	public MobileDeviceFormV9(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		fileChooser = new JFileChooser();
		if (src == menuFile_Open) {
			int returnval = fileChooser.showOpenDialog(MobileDeviceFormV9.this);

			if (returnval == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				JOptionPane.showMessageDialog(this, "menuFile_Opening file " + file.getName());
			} else {
				JOptionPane.showMessageDialog(this, "menuFile_Open command cancelled by user ");
			}

		} else if (src == menuFile_Save) {
			int returnval = fileChooser.showSaveDialog(MobileDeviceFormV9.this);

			if (returnval == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				JOptionPane.showMessageDialog(this, "Saving file " + file.getName());
			} else {
				JOptionPane.showMessageDialog(this, "menuFile_Save command cancelled by user ");
			}
		} else if (src == menuFile_Exit) {
			System.exit(0);
		} else if (src == menuItem_ColorRed) {
			reviewTextArea.setBackground(Color.RED);
		} else if (src == menuItem_ColorGreen) {
			reviewTextArea.setBackground(Color.GREEN);
		} else if (src == menuItem_ColorBlue) {
			reviewTextArea.setBackground(Color.BLUE);
		} else if (src == menuItem_ColorCustom) {
			Color customColor = JColorChooser.showDialog(MobileDeviceFormV9.this, "Choose Color",
					reviewTextArea.getBackground());
			reviewTextArea.setBackground(customColor);
		}
	}

	@Override
	public void addListeners() {
		super.addListeners();
		menuFile_Open.addActionListener(this);
		menuFile_Save.addActionListener(this);
		menuFile_Exit.addActionListener(this);
		menuItem_ColorRed.addActionListener(this);
		menuItem_ColorGreen.addActionListener(this);
		menuItem_ColorBlue.addActionListener(this);
		menuItem_ColorCustom.addActionListener(this);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV9 mobileDeviceForm9 = new MobileDeviceFormV9("Mobile Device Form V9");
		mobileDeviceForm9.addMenus();
		mobileDeviceForm9.addComponents();
		mobileDeviceForm9.setFrameFeatures();
		mobileDeviceForm9.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
