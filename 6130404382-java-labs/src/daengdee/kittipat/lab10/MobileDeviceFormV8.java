package daengdee.kittipat.lab10;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 22, 2019
 */
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV8 extends MobileDeviceFormV7 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JMenuItem menuItem_ColorCustom;

	public MobileDeviceFormV8(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	protected void addSubMenus() {
		super.addSubMenus();
		menuItem_ColorCustom = new JMenuItem("Custom ...");
		subMenuConfig_Color.add(menuItem_ColorCustom);

		menuFile.setMnemonic(KeyEvent.VK_F);
		menuConfig.setMnemonic(KeyEvent.VK_C);
		subMenuConfig_Color.setMnemonic(KeyEvent.VK_L);
		menuFile_new.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		menuFile_Open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		menuFile_Save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		menuFile_Exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
		menuItem_ColorBlue.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
		menuItem_ColorGreen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
		menuItem_ColorRed.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		menuItem_ColorCustom.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV8 mobileDeviceForm8 = new MobileDeviceFormV8("Mobile Device Form V8");
		mobileDeviceForm8.addMenus();
		mobileDeviceForm8.addComponents();
		mobileDeviceForm8.setFrameFeatures();
		mobileDeviceForm8.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
