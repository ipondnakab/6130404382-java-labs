package daengdee.kittipat.lab4;
/* Author: Kittipat Daengdee 
 * ID: 613040438-2 
 * Sec: 1 
 * Date: February 10, 2019
 * 
 * Interface Refuelable has one method called refuel().
 * 
 */
public interface Refuelable {
	void refuel();
}
