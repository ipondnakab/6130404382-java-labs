package daengdee.kittipat.lab4;
/* Author: Kittipat Daengdee 
 * ID: 613040438-2 
 * Sec: 1 
 * Date: February 10, 2019
 * 
 * Interface Moveable has three methods. The methods are accelerate(), brake() and setSpeed().

 */
public interface Moveable {
	void accelerate();
	void brake();
	void setSpeed(int speed);

}
