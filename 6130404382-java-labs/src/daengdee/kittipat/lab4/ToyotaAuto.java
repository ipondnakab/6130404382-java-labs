package daengdee.kittipat.lab4;
/* Author: Kittipat Daengdee 
 * ID: 613040438-2 
 * Sec: 1 
 * Date: February 10, 2019
 * 
 * Write a java class called ToyotaAuto
 * 
 * extends from Automobile. 
 * 
 * implements interface Movable and Refuelable. 
 */
class ToyotaAuto extends Automobile implements Moveable , Refuelable {
	/*has one public constructor that accepts int for maximum speed, 
	 * int for acceleration  and String for the model. Set gasoline to 100.
	 */
	public ToyotaAuto (int maxSpeed, int acceleration ,String model) {
		this.setMaxSpeed(maxSpeed);
		this.setAcceleration(acceleration);
		this.setModel(model);
		this.setGasoline(100);
	}

	/*implements the method refuel() that sets 
	 * the value of gasoline to 100 and 
	 * displays a message �refuels� */
	@Override
	public void refuel() {
		this.setGasoline(100);
		System.out.println(getModel() + " refuels");
	}
	
	/*implements the method accelerate() that increases the current speed by the acceleration. 
	 * However the method must first check if the new speed does not exceed the maximum speed. 
	 * Otherwise set the new speed to the maximum speed. The 
	 * method decreases gasoline by 15. The method also displays message �accelerates�
	 */
	@Override
	public void accelerate() {
		this.setSpeed(this.getSpeed() + this.getAcceleration());
		if (this.getSpeed() >= this.getMaxSpeed()) {
			this.setSpeed(this.getMaxSpeed());
		}
		this.setGasoline(this.getGasoline()-15);
		System.out.println(getModel() + " accelerates");
		
	}
	
	/*implements the method brake() that decreases the current speed by the acceleration. 
	 * The method must check if the new speed is not less than 0. Otherwise set the new speed to 0. 
	 * The method decreases the value of gasoline by 15. The method also displays message �brakes� 
	 */
	@Override
	public void brake() {
		this.setSpeed(this.getSpeed() - this.getAcceleration());
		if (this.getSpeed() <= 0 ) {
			this.setSpeed(this.getMaxSpeed());
		}
		this.setGasoline(this.getGasoline()-15);
		System.out.println(getModel() + " brake");
	}
	
	/*implements the method setSpeed(int) that accepts one integer for setting the speed. 
	 * The method must check if the input speed must neither be negative nor exceed maxSpeed. 
	 * Otherwise set speed to 0 or maxSpeed accordingly.*/
	@Override
	public void setSpeed(int speed) {
		if(speed >= this.getMaxSpeed()) {
			this.setSpeeds(this.getMaxSpeed());
		}else if (speed <= 0) {
			this.setSpeeds(0);
		}else {
			this.setSpeeds(speed);
		}
	}
	
	@Override
	public String toString() {
		return getModel() + " Gas:" + getGasoline() + " Speed:" + getSpeed() + "  Max speed:" + getMaxSpeed()
				+ " Acceleration:" + getAcceleration();
	}

		
}

