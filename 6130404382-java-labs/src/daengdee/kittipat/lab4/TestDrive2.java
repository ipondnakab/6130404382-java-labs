package daengdee.kittipat.lab4;
/* Author: Kittipat Daengdee 
 * ID: 613040438-2 
 * Sec: 1 
 * Date: February 10, 2019
 * 
 * has the main method as shown in Figure 3 and the output as shown in Figure 4
 * has method isFaster() that compares if the first automobile is faster than the
 * second automobile as shown in Figure 3 and 4.
 */
public class TestDrive2 {
	public static void isFaster(Object car1,Object car2) {
		int speedCar1 = ((Automobile) car1).getSpeed();
		int speedCar2 = ((Automobile) car2).getSpeed();
		String modelCar1 = ((Automobile) car1).getModel();
		String modelCar2 = ((Automobile) car2).getModel();
		if (speedCar1 < speedCar2) {
			System.out.println(modelCar1 + " is faster then " + modelCar2);
		}else if (speedCar1 > speedCar2){
			System.out.println(modelCar1 + " is NOT faster then " + modelCar2);
		}else {
			System.out.println(modelCar1 + " has the same speed as " + modelCar2);
		}
	}
	public static void main(String[] args) {
		ToyotaAuto carl = new ToyotaAuto(200, 10, "Vios"); 
		HondaAuto car2 = new HondaAuto(220, 8, "City"); 
		
		System.out.println(carl); 
		System.out.println(car2); 
		
		carl.accelerate(); 
		car2.accelerate(); 
		car2.accelerate();
		
		System.out.println(carl); 
		System.out.println(car2); 
		
		carl.brake(); 
		carl.brake();
		car2.brake();
		
		System.out.println(carl); 
		System.out.println(car2); 
		
		carl.refuel(); 
		car2.refuel();
		
		System.out.println(carl); 
		System.out.println(car2); 
		
		isFaster(carl, car2);
		isFaster(car2, carl);

	}

}
