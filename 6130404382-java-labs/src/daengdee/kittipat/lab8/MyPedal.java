package daengdee.kittipat.lab8;
/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 3, 2019
 */
import java.awt.geom.Rectangle2D;

public class MyPedal extends Rectangle2D.Double {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private final static int pedalWidth = 100;
	private final static int pedalHeight = 10;

	public MyPedal(int x, int y) {
		super(x, y, getPedalwidth(), getPedalheight());
	}

	public static int getPedalwidth() {
		return pedalWidth;
	}

	public static int getPedalheight() {
		return pedalHeight;
	}
}
