package daengdee.kittipat.lab8;
/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 3, 2019
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV2 extends MyCanvas {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	MyBall ball;
	MyBrick brick;
	MyPedal pedal;

	public MyCanvasV2() {
		ball = new MyBall(WIDTH / 2 - 15, HEIGHT / 2 - 15);
		brick = new MyBrick(WIDTH / 2 - 40, 0);
		pedal = new MyPedal(WIDTH / 2 - 50, HEIGHT - 10);

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, WIDTH, HEIGHT);

		g2d.fillRect(0, 0, WIDTH, HEIGHT);
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
		g2d.fill(brick);
		g2d.fill(pedal);
		g.drawLine(0, HEIGHT / 2, WIDTH, HEIGHT / 2);
		g.drawLine(WIDTH / 2, 0, WIDTH / 2, HEIGHT);

	}

}
