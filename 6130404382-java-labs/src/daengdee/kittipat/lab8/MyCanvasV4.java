package daengdee.kittipat.lab8;
/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 3, 2019
 */
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV4 extends MyCanvasV3 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static Color[] colorList = { Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.CYAN, Color.BLUE,
			Color.MAGENTA };
	static MyBall ballOnPedal = new MyBall(400 - 15, 600 - 40);

	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		g2d.setColor(Color.WHITE);
		for (int j = 0; j < 7; j++) {
			for (int i = 0; i < 10; i++) {
				brick[i] = new MyBrick(80 * i, 100 + 20 * j);
				g2d.setColor(colorList[j]);
				g2d.fill(brick[i]);
				g2d.setStroke(new BasicStroke(5));
				g2d.setColor(Color.BLACK);
				g2d.draw(brick[i]);

			}
		}

		g2d.setColor(Color.GRAY);
		g2d.fill(pedal);
		g2d.setColor(Color.WHITE);
		g2d.fill(ballOnPedal);

	}
}
