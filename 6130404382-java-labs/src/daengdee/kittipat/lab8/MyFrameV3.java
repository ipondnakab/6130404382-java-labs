package daengdee.kittipat.lab8;
/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 3, 2019
 */
import javax.swing.SwingUtilities;

public class MyFrameV3 extends MyFrameV2 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyFrameV3(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MyFrameV3 msw = new MyFrameV3("My Frame V3");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	protected void addComponents() {
		add(new MyCanvasV3());
	}
}
