package daengdee.kittipat.lab8;
/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 3, 2019
 */
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MyFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyFrame(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MyFrame msw = new MyFrame("My Frame");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	protected void addComponents() {
		add(new MyCanvas());
	}

	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
