package daengdee.kittipat.lab8;
/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 3, 2019
 */
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV3 extends MyCanvasV2 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	MyBall[] ball;
	MyBrick[] brick;

	public MyCanvasV3() {
		ball = new MyBall[4];
		ball[0] = new MyBall(0, 0);
		ball[1] = new MyBall(0, HEIGHT - 30);
		ball[2] = new MyBall(WIDTH - 30, 0);
		ball[3] = new MyBall(WIDTH - 30, HEIGHT - 30);

		brick = new MyBrick[10];
		for (int i = 0; i < 10; i++) {
			brick[i] = new MyBrick(80 * i, HEIGHT / 2 - 10);
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, WIDTH, HEIGHT);

		g2d.fillRect(0, 0, WIDTH, HEIGHT);
		g2d.setColor(Color.WHITE);
		for (int i = 0; i < 4; i++) {
			g2d.fill(ball[i]);
		}
		for (int i = 0; i < 10; i++) {
			g2d.setColor(Color.WHITE);
			g2d.fill(brick[i]);
			g2d.setStroke(new BasicStroke(5));
			g2d.setColor(Color.BLACK);
			g2d.draw(brick[i]);
		}

	}
}
