package daengdee.kittipat.lab8;
/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 3, 2019
 */
import java.awt.geom.Rectangle2D;

public class MyBrick extends Rectangle2D.Double {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static int brickWidth = 80;
	protected final static int brickHeight = 20;

	public MyBrick(int x, int y) {
		super(x, y, getBrickwidth(), brickHeight);

	}

	public static int getBrickwidth() {
		return brickWidth;
	}

	public static int getBrickheight() {
		return brickHeight;
	}
}
