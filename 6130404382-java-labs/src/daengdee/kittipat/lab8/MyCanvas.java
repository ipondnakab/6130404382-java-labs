package daengdee.kittipat.lab8;
/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 3, 2019
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

public class MyCanvas extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected final int WIDTH = 800;
	protected final int HEIGHT = 600;

	public void paintComponent(Graphics g) {
		super.paintComponent(g); // paint parent's background
		g.setColor(Color.WHITE);
		Graphics2D g2d = (Graphics2D) g;

		Shape circleFace = new Ellipse2D.Double((WIDTH / 2) - 150, (HEIGHT / 2) - 150, 2 * 150, 2 * 150);
		Shape circleEye1 = new Ellipse2D.Double((WIDTH / 2) - 15 - 40, (HEIGHT / 2) - 30 - 30, 2 * 15, 2 * 30);
		Shape circleEye2 = new Ellipse2D.Double((WIDTH / 2) - 15 + 40, (HEIGHT / 2) - 30 - 30, 2 * 15, 2 * 30);
		Shape squareMouth = new Rectangle2D.Double((WIDTH / 2) - 50, (HEIGHT / 2) - 5 + 80, 2 * 50, 2 * 5);

		g2d.draw(circleFace);
		g2d.draw(circleEye1);
		g2d.draw(circleEye2);
		g2d.draw(squareMouth);
		g2d.fill(circleEye1);
		g2d.fill(circleEye2);
		g2d.fill(squareMouth);
	}

	public MyCanvas() {
		super();
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setBackground(Color.black);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
