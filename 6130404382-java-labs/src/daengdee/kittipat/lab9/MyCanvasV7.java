package daengdee.kittipat.lab9;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 10, 2019
 */
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import daengdee.kittipat.lab8.MyBall;
import daengdee.kittipat.lab8.MyBrick;

public class MyCanvasV7 extends MyCanvasV6 implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected int numBrick = super.WIDTH / MyBrick.getBrickwidth();
	protected MyBrickV2[] brick = new MyBrickV2[numBrick];
	MyBallV2 ball3 = new MyBallV2(0, 0);
	Thread running3 = new Thread(this);

	public MyCanvasV7() {
		ball3.ballVelX = 1;
		ball3.ballVelY = 1;
		for (int i = 0; i < numBrick; i++) {
			brick[i] = new MyBrickV2(MyBrick.getBrickwidth() * i, super.HEIGHT / 2);
			brick[i].visible = true;
		}
		running3.start();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, this.getWidth(), this.getHeight()));
		g2d.setColor(Color.WHITE);
		g2d.fill(ball3);

		for (int i = 0; i < numBrick; i++) {
			if (brick[i].visible) {
				g2d.setColor(Color.RED);
				g2d.fill(brick[i]);
				g2d.setStroke(new BasicStroke(5));
				g2d.setColor(Color.BLUE);
				g2d.draw(brick[i]);
			} else {
				// clearRect(brick[i].x, brick[i].y, MyBrickV2.getBrickwidth(),
				// MyBrickV2.getBrickheight());
				brick[i] = new MyBrickV2(-1000, -1000);
				// g.clearRect(0, 0, getWidth(), getHeight() );
//				g2d.clearRect(brick[i].x, brick[i].y, MyBrickV2.getBrickwidth(), MyBrickV2.getBrickheight());
			}
		}
	}

	public void run() {
		while (true) {
			if (ball3.x <= 0 || ball3.x >= super.WIDTH - MyBall.diameter) {
				ball3.ballVelX *= -1;
			}

			if (ball3.y <= 0 || ball3.y >= super.HEIGHT - MyBall.diameter) {
				ball3.ballVelY *= -1;
			}
			for (int i = 0; i < numBrick; i++) {
				checkCollision(ball3, brick[i]);

			}
			ball3.move();

			repaint();

			try {
				Thread.sleep(10);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}
	}

	public void checkCollision(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.getBrickwidth()));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.getBrickheight()));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;

		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) { // collides top or bottom
				// Change the direction on y coordinate to the opposite
				ball.ballVelY *= -1;

			} else { // collides left or right
				// Change the direction on x coordinate to the opposite
				ball.ballVelX *= -1;
			}

			brick.visible = false;

		}

	}

}
