package daengdee.kittipat.lab9;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 10, 2019
 */
import javax.swing.SwingUtilities;

public class MyFrameV7 extends MyFrameV6 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyFrameV7(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public void addComponents() {
		add(new MyCanvasV7());

	}

	public static void createAndShowGUI() {
		MyFrameV7 msw7 = new MyFrameV7("My Frame V7");
		msw7.addComponents();
		msw7.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				createAndShowGUI();
			}
		});
	}

}
