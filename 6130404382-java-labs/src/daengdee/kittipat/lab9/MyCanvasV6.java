package daengdee.kittipat.lab9;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 10, 2019
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import daengdee.kittipat.lab8.MyBall;

public class MyCanvasV6 extends MyCanvasV5 implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	MyBallV2 ball2 = new MyBallV2(super.WIDTH - MyBall.diameter, super.HEIGHT - MyBall.diameter);
	Thread running2 = new Thread(this);

	public MyCanvasV6() {
		ball2.ballVelX = 1;
		ball2.ballVelY = 1;
		running2.start();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, this.getWidth(), this.getHeight()));
		g2d.setColor(Color.WHITE);
		g2d.fill(ball2);
	}

	public void run() {
		while (true) {
			if (ball2.x <= 0 || ball2.x >= super.WIDTH - MyBall.diameter) {
				ball2.ballVelX *= -1;
			}

			if (ball2.y <= 0 || ball2.y >= super.HEIGHT - MyBall.diameter) {
				ball2.ballVelY *= -1;
			}

			ball2.move();

			repaint();

			try {
				Thread.sleep(10);
			} catch (InterruptedException ex) {

			}
		}
	}

}
