package daengdee.kittipat.lab9;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 10, 2019
 */
import daengdee.kittipat.lab8.MyPedal;

public class MyPedalV2 extends MyPedal {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// declare speedPedal
	protected static int speedPedal = 30;

	public MyPedalV2(int x, int y) {
		super(x, y);
		// TODO Auto-generated constructor stub
	}

	// And the code for moveLeft()
	public void moveLeft() {
		if (x - speedPedal < 0) {
			x = 0;

		} else {
			x -= speedPedal;
		}

	}

	// And the code for moveRight()
	public void moveRight() {
		if (x + speedPedal > 800 - MyPedal.getPedalheight()) {
			x = 800 - MyPedal.getPedalwidth();
		} else {
			x += speedPedal;
		}
	}
}
