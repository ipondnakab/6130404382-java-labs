package daengdee.kittipat.lab9;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 10, 2019
 */
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;

import daengdee.kittipat.lab8.MyBall;
import daengdee.kittipat.lab8.MyBrick;
import daengdee.kittipat.lab8.MyPedal;

public class MyCanvasV8 extends MyCanvasV7 implements Runnable, KeyListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected int numCol = super.WIDTH / MyBrick.getBrickwidth();
	protected int numRow = 7;
	protected int numVisibleBricks = numCol * numRow;
	protected MyBrickV2[][] bricks;
	protected Thread running;
	protected Color[] color = { Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.ORANGE,
			Color.RED };
	protected MyPedalV2 pedal;
	protected int lives;
	protected MyBallV2 ball4 = new MyBallV2((super.WIDTH / 2) - (MyBall.getDiameter() / 2) - 15,
			super.HEIGHT - MyBall.getDiameter() - MyPedal.getPedalheight() - 30);

	public MyCanvasV8() {
		// initialize bricks with numRow and numCol
		bricks = new MyBrickV2[numRow][numCol];

		ball4.ballVelX = 2;
		ball4.ballVelY = 2;

		running = new Thread(this);

		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				bricks[i][j] = new MyBrickV2(j * MyBrick.getBrickwidth(),
						super.HEIGHT / 3 - i * MyBrick.getBrickheight());
			}
		}

		pedal = new MyPedalV2(super.WIDTH / 2 - MyPedal.getPedalwidth() / 2, super.HEIGHT - MyPedal.getPedalheight());
		running.start();
		setFocusable(true);
		addKeyListener(this);

		lives = 3;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));

		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				if (bricks[i][j].visible) {
					g2d.setColor(color[i]);
					g2d.fill(bricks[i][j]);
					g2d.setStroke(new BasicStroke(4));
					g2d.setColor(Color.BLACK);
					g2d.draw(bricks[i][j]);
				} else {
					bricks[i][j] = new MyBrickV2(-1000, -1000);
				}
			}
		}

		// Draw a ball
		g2d.setColor(Color.WHITE);
		g2d.fill(this.ball4);

		// Draw a pedal
		g2d.setColor(Color.GRAY);
		g2d.fill(pedal);

		// Draw score
		g2d.setFont(new Font("SanSerif", Font.BOLD, 20));
		g2d.setColor(Color.BLUE);
		String s = " Lives : " + lives;
		g2d.drawString(s, 10, 30);

		g2d.setFont(new Font("SanSerif", Font.BOLD, 80));
		// Draw a string You Won
		if (numVisibleBricks == 0) {
			g2d.setColor(Color.GREEN);
			g2d.drawString("You won", super.WIDTH / 2 - 160, super.HEIGHT / 2);

		}

		// draw a string GAME OVER
		if (lives <= 0) {
			g2d.setColor(Color.GRAY);
			g2d.drawString("GAME OVER", super.WIDTH / 2 - 250, super.HEIGHT / 2);
			ball4.x = super.WIDTH / 2 - MyBall.getDiameter() / 2 - 15;
			ball4.y = super.HEIGHT - MyBall.getDiameter() - MyPedal.getPedalheight() - 30;
			pedal.x = super.WIDTH / 2 - MyPedal.getPedalwidth() / 2;
			pedal.y = super.HEIGHT - MyPedal.getPedalheight();
		}

	}

	private void checkPassBottom() {
		if (ball4.y >= super.HEIGHT) {
			ball4.x = pedal.x + MyPedal.getPedalwidth() / 2 - MyBall.getDiameter() / 2 - 15;
			ball4.y = super.HEIGHT - MyBall.getDiameter() - MyPedal.getPedalheight() - 30;
			ball4.ballVelX = 0;
			ball4.ballVelY = 0;
			lives--;
			repaint();
		}

	}

	public void checkCollision(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.getDiameter() / 2.0;
		double y = ball.y + MyBall.getDiameter() / 2.0;
		double deltax = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.getBrickwidth()));
		double deltay = y - Math.max(brick.y, Math.min(x, brick.y + MyBrick.getBrickheight()));

		boolean collided = (deltax * deltax + deltay * deltay) < (MyBall.getDiameter() * MyBall.getDiameter() / 4.0);

		if (collided) {
			if (deltax * deltax < deltay * deltay) {
				ball.ballVelY *= -1;
			} else {
				ball.ballVelX *= -1;
			}

			ball.move();
			brick.visible = false;
		}

	}

	private void collideWithPedal(MyBallV2 ball, MyPedalV2 pedal) {
		double x = ball.x + MyBall.getDiameter() / 2.0;
		double y = ball.y + MyBall.getDiameter() / 2.0;
		double deltax = x - Math.max(pedal.x, Math.min(x, pedal.x + MyPedalV2.getPedalwidth()));
		double deltay = y - Math.max(pedal.y, Math.min(x, pedal.y + MyPedalV2.getPedalheight()));
		boolean collided = (deltax * deltax + deltay * deltay) < (MyBall.getDiameter() * MyBall.getDiameter() / 4.0);
		if (collided) {
			if (deltax * deltax < deltay * deltay) {
				ball.ballVelY *= -1;
			} else {
				ball.ballVelX *= -1;
			}
			ball.move();
		}
	}

	public void run() {
		while (true) {
			if (ball4.x + ball4.ballVelX < 0 || ball4.x + ball4.ballVelX > super.WIDTH - MyBall.getDiameter()) {
				ball4.ballVelX *= -1;
			}
			if (ball4.y + ball4.ballVelY < 0) {
				ball4.ballVelY *= -1;
			} else {

				ball4.move();
			}

			for (int i = 0; i < numRow; i++) {
				for (int j = 0; j < numCol; j++) {
					checkCollision(ball4, bricks[i][j]);
				}
			}

			collideWithPedal(ball4, pedal);
			checkPassBottom();
			repaint();

			try {
				Thread.sleep(10);
			} catch (InterruptedException ex) {
//				ex.printStackTrace();
			}
		}

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		if (arg0.getKeyCode() == KeyEvent.VK_LEFT) { // check if press left arrow
			// move left
			pedal.moveLeft();
		} else if (arg0.getKeyCode() == KeyEvent.VK_RIGHT) { // check if press right arrow
			// move right
			pedal.moveRight();
		} else if (arg0.getKeyCode() == KeyEvent.VK_SPACE) { // check if press space bar
			// set ballVelY to 4
			ball4.ballVelY = 4;

			// then randomly set ballVelX to 4 or -4
			int random = (int) (Math.random() * 100);
			if (random > 50) {
				ball4.ballVelX = 4;
			} else {
				ball4.ballVelX = -4;
			}
		}
		repaint();

	}

}
