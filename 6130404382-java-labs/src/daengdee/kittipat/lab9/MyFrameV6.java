package daengdee.kittipat.lab9;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 10, 2019
 */
import javax.swing.SwingUtilities;

public class MyFrameV6 extends MyFrameV5 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyFrameV6(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public void addComponents() {
		add(new MyCanvasV6());

	}

	public static void createAndShowGUI() {
		MyFrameV6 msw6 = new MyFrameV6("My Frame V6");
		msw6.addComponents();
		msw6.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				createAndShowGUI();
			}
		});
	}

}
