package daengdee.kittipat.lab9;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 10, 2019
 */
import javax.swing.SwingUtilities;

public class MyFrameV8 extends MyFrameV7 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyFrameV8(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public void addComponents() {
		add(new MyCanvasV8());

	}

	public static void createAndShowGUI() {
		MyFrameV8 msw8 = new MyFrameV8("My Frame V8");
		msw8.addComponents();
		msw8.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				createAndShowGUI();
			}
		});
	}
}
