package daengdee.kittipat.lab9;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 10, 2019
 */
import daengdee.kittipat.lab8.MyBall;

public class MyBallV2 extends MyBall {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected int ballVelX;
	protected int ballVelY;

	public MyBallV2(int x, int y) {
		super(x, y);
		// TODO Auto-generated constructor stub
	}

	public void move() {
		x += ballVelX;
		y += ballVelY;
		System.out.println(x + "," + y);
	}

}
