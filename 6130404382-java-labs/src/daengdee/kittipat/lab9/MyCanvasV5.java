package daengdee.kittipat.lab9;

/**
 * Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 
 * Date: April 10, 2019
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import daengdee.kittipat.lab8.MyBall;
import daengdee.kittipat.lab8.MyCanvasV4;

public class MyCanvasV5 extends MyCanvasV4 implements Runnable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected MyBallV2 ball = new MyBallV2(0, HEIGHT / 2 - MyBall.diameter / 2);
	protected Thread running = new Thread(this);

	public MyCanvasV5() {
		ball.ballVelX = 2;
		ball.ballVelY = 0;
		running.start();

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, this.getWidth(), this.getHeight()));
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);

	}

	public void run() {

		while (true) {
			if (ball.x + ball.ballVelX + MyBall.diameter > super.WIDTH) {
				ball.ballVelX = 0;
				break;
			} else {
				ball.move();
			}

			repaint();
			try {
				Thread.sleep(10);
			} catch (InterruptedException ex) {

			}

		}

	}

}
