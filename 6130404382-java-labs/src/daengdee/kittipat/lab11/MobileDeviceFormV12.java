package daengdee.kittipat.lab11;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import daengdee.kittipat.lab5.MobileDevice;

public class MobileDeviceFormV12 extends MobileDeviceFormV11  implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MobileDeviceFormV12(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	protected FileWriter fileWriter;
	public void addListeners() {
		super.addListeners();
		menuFile_Open.addActionListener(this);
		menuFile_Save.addActionListener(this);
	}	
	@SuppressWarnings("unchecked")
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == menuFile_Save) {
			int returnval = fileChooser.showSaveDialog(fileChooser);
			if (returnval == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				JOptionPane.showMessageDialog (null, "Saving file:  " + file.getName() + ".txt\n", "Message", JOptionPane.PLAIN_MESSAGE);
				try {
					FileOutputStream fileOutputStream = new FileOutputStream(file);
					ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
					objectOutputStream.writeObject((ArrayList<MobileDevice>) mobileArrayList);
					objectOutputStream.close();
				} catch (Exception e) {

				}
				
			} else {
	        	JOptionPane.showMessageDialog (null,"Save command cancelled by user.\n", "Message", JOptionPane.PLAIN_MESSAGE);
	        }
		}
		else if (src == menuFile_Open) {
			int retval = fileChooser.showOpenDialog(fileChooser);
			if (retval == JFileChooser.APPROVE_OPTION) {
				try {
					File file = fileChooser.getSelectedFile();	
					file.getAbsoluteFile();
					FileInputStream fileInput = new FileInputStream(file);
					ObjectInputStream dataInput = new ObjectInputStream(fileInput);
					mobileArrayList = (ArrayList<MobileDevice>) dataInput.readObject();
					dataInput.close();
					fileInput.close();
				}
				catch (Exception e){
					JOptionPane.showMessageDialog(null, "Can't open this file. Please try again.", "Message", JOptionPane.PLAIN_MESSAGE);
				}
			}
		}
	}
	

	public static void createAndShowGUI() {
		MobileDeviceFormV12 mobileDeviceFormV12 = new MobileDeviceFormV12("Mobile Device FormV12");
		mobileDeviceFormV12.addComponents();
		mobileDeviceFormV12.addMenus();
		mobileDeviceFormV12.setFrameFeatures();
		mobileDeviceFormV12.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
