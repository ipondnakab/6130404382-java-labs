package daengdee.kittipat.lab11;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import daengdee.kittipat.lab10.MobileDeviceFormV9;
import daengdee.kittipat.lab5.MobileDevice;

public class MobileDeviceFormV10 extends MobileDeviceFormV9 {
	protected JMenu dataMunu;
	protected JMenuItem displayMI, sortMI, searchMI, removeMI;
	protected String displayMobileList = "", displaySortMobileList = "", search, remove;

	private static final long serialVersionUID = 1L;

	public MobileDeviceFormV10(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	ArrayList<MobileDevice> mobileArrayList = new ArrayList<MobileDevice>();
	ArrayList<MobileDevice> sortMobileArrayList = new ArrayList<MobileDevice>();

	public void addMObileDevice() {
		// add mobile in mobileArrayList
		mobileArrayList.add(new MobileDevice(modelNameTextField.getText(), OSSelec, Integer.valueOf(priceTextField.getText()), Integer.valueOf(weightTextField.getText())));
		System.out.println(mobileArrayList);

	}

	public void addListeners() {
		super.addListeners();
		displayMI.addActionListener(this);
		sortMI.addActionListener(this);
		searchMI.addActionListener(this);
		removeMI.addActionListener(this);
	}

	protected void handleOKButton() {
		super.handleOKButton();
		addMObileDevice();
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		if (src == okButton) {
			handleOKButton();
		} else if (src == cancelButton) {
			handleCancelButton();
		} else if (src == displayMI) {
			handleDisplayMI();
		} else if (src == sortMI) {
			handleSortMI();
		} else if (src == searchMI) {
			handleSearchMI();
		} else if (src == removeMI) {
			handleRemoveMI();
		} else {
			super.actionPerformed(event);
		}
	}

	// user clicks the menu item �Remove�
	protected void handleRemoveMI() {
		remove = JOptionPane.showInputDialog(this, "Please input model name to remove: ", "input",
				JOptionPane.INFORMATION_MESSAGE);
		// try stop the exception
		try {
			// Can count the value that the mobile finds, if found to be +1, if not found,
			// will show that NOT found.
			int countFound = 0;
			for (int i = 0; i < mobileArrayList.size(); i++) {
				if (remove.equalsIgnoreCase(mobileArrayList.get(i).getModelName())) { // NullPointerException may occur
					// Shown dialog message if the model is removed
					JOptionPane.showMessageDialog(this, mobileArrayList.get(i).toString() + "\n is removed.");
					mobileArrayList.remove(i); //
					countFound += 1; // count the value that the mobile finds
				}
			}
			// not found the value = 0 default
			if (countFound == 0) {
				JOptionPane.showMessageDialog(this, remove + "\n is NOT found");
			}
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
	}

	// user clicks the menu item �Search�
	protected void handleSearchMI() {
		search = JOptionPane.showInputDialog(this, "Please input model name to search: ", "input",
				JOptionPane.INFORMATION_MESSAGE);
		// try stop the exception
		try {
			// Can count the value that the mobile finds, if found to be +1, if not found,
			// will show that NOT found.
			int countFound = 0;
			for (int i = 0; i < mobileArrayList.size(); i++) {
				if (search.equalsIgnoreCase(mobileArrayList.get(i).getModelName())) { // NullPointerException may occur
					JOptionPane.showMessageDialog(this, mobileArrayList.get(i).toString() + "\n is found");
					countFound += 1; // count the value that the mobile finds
				}
			}
			// not found the value = 0 default
			if (countFound == 0) {
				JOptionPane.showMessageDialog(this, search + "\n is NOT found");
			}
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
	}

	// user clicks the menu item �Search�
	@SuppressWarnings("unchecked")
	protected void handleSortMI() {
		sortMobileArrayList = (ArrayList<MobileDevice>) mobileArrayList.clone();
		Collections.sort(sortMobileArrayList);

		// show SortMobile in Message
		for (int i = 0; i < sortMobileArrayList.size(); i++) {
			displaySortMobileList += (i + 1) + ":  " + sortMobileArrayList.get(i).toString() + "\n";
		}
		JOptionPane.showMessageDialog(this, displaySortMobileList);
		displaySortMobileList = "";
	}

	// user clicks the menu item �Display�
	protected void handleDisplayMI() {
		// show Mobile in Message
		for (int i = 0; i < mobileArrayList.size(); i++) {
			displayMobileList += (i + 1) + ":  " + mobileArrayList.get(i).toString() + "\n";
		}
		JOptionPane.showMessageDialog(this, displayMobileList);
		displayMobileList = "";

	}

	protected void addMenus() {
		super.addMenus();
		dataMunu = new JMenu("Data");
		displayMI = new JMenuItem("Display");
		sortMI = new JMenuItem("Sort");
		searchMI = new JMenuItem("Search");
		removeMI = new JMenuItem("Remove");

		menuBar.add(dataMunu);
		dataMunu.add(displayMI);
		dataMunu.add(sortMI);
		dataMunu.add(searchMI);
		dataMunu.add(removeMI);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV10 mobileDeviceFormV10 = new MobileDeviceFormV10("Mobile Device Form V10");
		mobileDeviceFormV10.addComponents();
		mobileDeviceFormV10.addMenus();
		mobileDeviceFormV10.setFrameFeatures();
		mobileDeviceFormV10.addListeners();

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
