package daengdee.kittipat.lab11;

import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionListener;

public class MobileDeviceFormV11 extends MobileDeviceFormV10 implements ActionListener, ListSelectionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MobileDeviceFormV11(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	protected void handleOKButton() {
		try {
			if (modelNameTextField.getText().equalsIgnoreCase("")) {
				JOptionPane.showMessageDialog(null, "Please enter model name");
			} else if (Integer.valueOf(weightTextField.getText()) < 100) {
				JOptionPane.showMessageDialog(null, "Too light: valid weight is [100, 3000]");
			} else if (Integer.valueOf(weightTextField.getText()) > 3000) {
				JOptionPane.showMessageDialog(null, "Too heavy: valid weight is [100, 3000]");
			} else {
				super.handleOKButton();
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Please enter only numeric input for weight");
		}
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV11 mobileDeviceFormV11 = new MobileDeviceFormV11("Mobile Device Form V11");
		mobileDeviceFormV11.addComponents();
		mobileDeviceFormV11.addMenus();
		mobileDeviceFormV11.setFrameFeatures();
		mobileDeviceFormV11.addListeners();

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
