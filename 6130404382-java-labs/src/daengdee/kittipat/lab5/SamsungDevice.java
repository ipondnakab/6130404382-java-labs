package daengdee.kittipat.lab5;

/**
 * This java program is SamsungDevice as a subclass of MobileDevice
 * Author:Kittipat Daengdee ID: 613040438-2 Sec: 1 Date: February 21, 2019
 */
//SamsungDevice as a subclass of MobileDevice (class SamsungDevice extends MobileDevice)
public class SamsungDevice extends MobileDevice {
	// Add the private static class member called brand and the public static
	// methods
	// and this class variable brand has the value as �Samsung�
	private static String brand = "Samsung";

	private double androidVersion;

	public static String getBrand() {
		return brand;
	}

	public static void setBrand(String brand) {
		SamsungDevice.brand = brand;
	}

	public double getAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(double androidVersion) {
		this.androidVersion = androidVersion;
	}

	public SamsungDevice(String modelName, int price, int weight, double androidVersion) {
		super(modelName, "Android", price, weight);
		this.androidVersion = androidVersion;
	}

	public SamsungDevice(String modelName, int price, double androidVersion) {
		super(modelName, "Android", price);
		this.androidVersion = androidVersion;
	}

	@Override
	public String toString() {
		return "SamsungDevice [Model name: " + this.getModelName() + ", OS: " + this.getOs() + ", price: "
				+ this.getPrice() + "Bath, weight: " + this.getWeight() + "g, Android Version: " + androidVersion + "]";
	}

	public void displayTime() {
		System.out.println("Display times in both using a digital format and using an analog watch");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
