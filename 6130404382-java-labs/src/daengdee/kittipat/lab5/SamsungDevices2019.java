package daengdee.kittipat.lab5;

/**
 * This java program is SamsungDevices2019 shows results is "I would like to
 * have" , "SamsungDevice [Model name: , OS: , Price: , Weight: , Android
 * Version:]","But to savs money , I should buy ", "Samsung Galaxy Note 9 is
 * more expensive than Sumsung Galaxy S9 by 1600.0Baht.", "Both these device
 * have the same beand which is Samsung" Author: Kittipat Daengdee
 * ID:613040438-2 Sec: 1 Date: February 21, 2019
 */
public class SamsungDevices2019 {

	public static void main(String[] args) {
		MobileDevice s9 = new SamsungDevice("Galaxy S9", 23900, 163, 8.0);
		SamsungDevice note9 = new SamsungDevice("Galaxy Note 9", 25500, 201, 8.1);
		System.out.println("I would like to have ");
		System.out.println(note9);
		System.out.println("But to save money, I should buy ");
		System.out.println(s9);
		double diff = note9.getPrice() - s9.getPrice();
		System.out.println("Samsung Galaxy Note 9 is more expensive" + " than Samsung Galaxy S9 by " + diff + " Baht.");
		System.out.println("Both these devices have the same brand which is " + SamsungDevice.getBrand());
	}

}
