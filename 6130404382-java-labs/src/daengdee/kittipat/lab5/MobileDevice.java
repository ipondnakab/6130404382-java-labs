package daengdee.kittipat.lab5;

/**
 * This java program is java class called MobileDevice. Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 Date: February 21, 2019
 */
public class MobileDevice implements Comparable<MobileDevice> {
	private String modelName;
	private String OS;
	private int price;
	private int weight;
	
	public MobileDevice(String modelName, String OS, int price, int weight) {
		this.modelName = modelName;
		this.OS = OS;
		this.price = price;
		this.weight = weight;
	}

	public MobileDevice(String modelName, String OS, int price) {
		this.modelName = modelName;
		this.OS = OS;
		this.price = price;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getOs() {
		return OS;
	}

	public void setOs(String OS) {
		this.OS = OS;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "MobileDevice [Model name: " + modelName + ", OS: " + OS + ", price: " + price + "Bath, weight: "
				+ weight + "g]";
	}

	public static void main(String[] args) {

	}

	@Override
	public int compareTo(MobileDevice mobileDevice) {
		// TODO Auto-generated method stub
		return this.price > mobileDevice.price ? 1 : this.price < mobileDevice.price ? -1 : 0;
	}

}
