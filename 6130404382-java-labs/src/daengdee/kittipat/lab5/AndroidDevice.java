package daengdee.kittipat.lab5;

/**
 * This java program is java class called AndroidDevice. Author: Kittipat
 * Daengdee ID: 613040438-2 Sec: 1 Date: February 21, 2019
 */
public abstract class AndroidDevice {

	abstract void usage();

	private static String OS = "Android";

	public static String getOs() {
		return OS;
	}

}
