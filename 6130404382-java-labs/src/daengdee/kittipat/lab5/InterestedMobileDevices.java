package daengdee.kittipat.lab5;

/**
 * This java program what is given is the program called InterestedMobileDevices
 * which consists of only a main() method. The main() method creates two objects
 * that are instances of class MobileDevice that you need to implement. This
 * java program is InterestedMobileDevices shows results is "MobileDevice [Model
 * name: , OS: , Price: , Weight: ]", "Apple iPad Mini 3 has new price as 11000
 * Baht." and "galaxy Note 9 has weight as 201 grams." Author: Kittipat Daengdee
 * ID: 613040438-2 Sec: 1 Date: February 21, 2019
 */
public class InterestedMobileDevices {

	public static void main(String[] args) {
		MobileDevice galaxyNote9 = new MobileDevice("Galaxy Note 9", "Android", 25500, 201);
		MobileDevice iPadGen6 = new MobileDevice("Apple iPad Mini 3", "iOS", 11500);
		System.out.println(galaxyNote9);
		System.out.println(iPadGen6);
		iPadGen6.setPrice(11000);
		System.out.println(iPadGen6.getModelName() + " has new price as " + iPadGen6.getPrice() + " Baht");
		System.out.println(galaxyNote9.getModelName() + " has " + " weight as " + galaxyNote9.getWeight() + " grams");
	}

}
