package daengdee.kittipat.lab5;

/**
 * This java program is TestPolymorphism shows results is "AndroidSmartWatch
 * [brand Name: , Model Name: , Price:]", "Display time only using a digital
 * format" , "SamsungDevice [Model name: , OS: , Price: , Weight: , Android
 * Version:]" and "Display times in both using a digital format and using an
 * analog watch" Author: Kittipat Daengdee ID: 613040438-2 Sec: 1 Date:February
 * 21, 2019
 */
public class TestPolymorphism {
	public static void main(String[] wigs) {
// TODO Auto-generated method stub 
		AndroidSmartWatch ticwatchPro = new AndroidSmartWatch("Mobvoi", "TicWatch Pro", 8390);
		SamsungDevice note9 = new SamsungDevice("Galaxy Note 9", 25500, 201, 8.1);
		System.out.println(ticwatchPro);
		ticwatchPro.displayTime();
		System.out.println();
		System.out.println(note9);
		note9.displayTime();
	}
}
