package daengdee.kittipat.lab5;

/**
 * This java program is AndroidSmartWatch that is a subclass of AndroidDevice
 * Author: Kittipat Daengdee ID: 613040438-2 Sec: 1 Date: February 21, 2019
 * 
 */
public class AndroidSmartWatch extends AndroidDevice {
	private String brandName, modelName;
	private int price;

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public AndroidSmartWatch(String brandName, String modelName, int price) {
		this.brandName = brandName;
		this.modelName = modelName;
		this.price = price;
	}

	@Override
	void usage() {
		System.out.println("AndroidSmartWatch Usage: Show time, date, " + "your heart rate, and your step count ");

	}

	public void displayTime() {
		System.out.println("Display time only using a digital format");
	}

	@Override
	public String toString() {
		return "AndroidSmartWatch [Brand Name:" + brandName + ", Model Name: " + modelName + ", Price: " + price
				+ " Bath]";
	}

}
